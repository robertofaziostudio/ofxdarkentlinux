#include "ofApp.h"
#include <cstdlib>

//--------------------------------------------------------------
void ofApp::setup(){

    /*std::system("env");
    std::cout << "----------------" << std::endl;
    std::system("ldd ml_demorf_debug");
    std::cout << "----------------" << std::endl;*/

    //std::string cfgfile = ofToDataPath( "cfg/yolo9000.cfg" );
    //std::string weightfile = ofToDataPath( "yolo9000.weights" );
    //std::string nameslist = ofToDataPath( "cfg/9k.names" );

    std::string cfgfile = ofToDataPath( "cfg/yolov2-tiny-voc.cfg" );
    std::string weightfile = ofToDataPath( "yolov2-tiny-voc.weights" );
    std::string nameslist = ofToDataPath( "cfg/voc.names" );
    darknet.init( cfgfile, weightfile, nameslist );

    cam.setDeviceID( 0 );
    cam.setDesiredFrameRate( 30 );

    tWebcam.addListener(this, &ofApp::useWebcam);
    tVideo.addListener(this, &ofApp::useVideo);

    gui.setup();
    gui.setName("YoloLive");
    gui.add(thresh.set("threshold", 0.24, 0.0, 1.0));
    gui.add(tWebcam.setup("webcam", false));
    gui.add(tVideo.setup("video", false));

}

//--------------------------------------------------------------
void ofApp::update(){
    if (tWebcam) {
            cam.update();
            detections = darknet.yolo(cam.getPixels(), thresh);
        }
        else if (tVideo) {
            movie.update();
            detections = darknet.yolo(movie.getPixels(), thresh);
        }
}

//--------------------------------------------------------------
void ofApp::draw(){
    // draw input source
    if (tWebcam) {
        cam.draw(0, 0);
    }
    else if (tVideo) {
        movie.draw(0, 0);
    }
    ofPushStyle();
    for(detected_object d : detections ){
        ofSetColor( d.color );
        glLineWidth( ofMap( d.probability, 0, 1, 0, 8 ) );
        ofNoFill();
        ofRectangle rect = d.rect;
        ofDrawRectangle( rect );
        ofDrawBitmapStringHighlight( d.label + ": " + ofToString(d.probability), rect.x, rect.y+20 );
    }
    ofPopStyle();
    gui.draw();
}

//--------------------------------------------------------------
void ofApp::useWebcam(bool & b) {
    if (!b) return;
    tVideo = false;
    cam.initGrabber(640, 480);
    movie.stop();
    movie.close();
}

//--------------------------------------------------------------
void ofApp::useVideo(bool & b) {
    if (!b) return;
    ofFileDialogResult result = ofSystemLoadDialog();
    if (result.bSuccess) {
        tWebcam = false;
        cam.close();
        movie.load(result.filePath);
        movie.play();
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){
//    screen.mouseMoved(x, y);
}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){
//    screen.mouseDragged(x, y);
}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
//    screen.mousePressed(x, y);
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){
//    screen.mouseReleased(x, y);
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
