#pragma once

#include "ofMain.h"

#include "ofxGui.h"
#include "ofxDarknet.h"
//#include "ofxScreenGrabCropped.h"
#include "ofxOpenCv.h"

class ofApp : public ofBaseApp{

public:

    void setup();
    void update();
    void draw();

    void useWebcam(bool & b);
    void useVideo(bool & b);

    void keyPressed(int key);
    void mouseMoved(int x, int y );
    void mouseDragged(int x, int y, int button);
    void mousePressed(int x, int y, int button);
    void mouseReleased(int x, int y, int button);
    void keyReleased(int key);
    void mouseEntered(int x, int y);
    void mouseExited(int x, int y);
    void windowResized(int w, int h);
    void dragEvent(ofDragInfo dragInfo);
    void gotMessage(ofMessage msg);

    ofxDarknet darknet;
    vector<detected_object> detections;

    ofVideoPlayer movie;
    ofVideoGrabber cam;
    ofParameter<float> thresh;

    ofxPanel gui;
    ofxToggle tWebcam, tVideo;

};
