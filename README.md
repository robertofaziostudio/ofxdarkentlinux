# ofxDarknetLinux

ofxDarknetLinux is a openFrameworks wrapper for darknet modified from https://github.com/schwittlick/ofxDarknet look also there for details

Darknet is an open source neural network framework written in C and CUDA. It is fast, easy to install, and supports CPU and GPU computation. http://pjreddie.com/darknet/

## Changes made
Sono stati aggiunti flag per permettere di compilare correttamente su Linux con cuda 10.1

## Setup

Install the dependencies for building darknet:
* [CUDA 10.1 64bit](https://developer.nvidia.com/cuda-downloads)
* [cuDNN](https://developer.nvidia.com/rdp/form/cudnn-download-survey)
* OpenCV 2

Then clone this repository in the addons directory of openframeworks and rename the directory created cloning this repository to ofxDarknet

### Probles with building

Be careful if you have installed other cuda versions, if cmake is finding cuda 10.1 properly you should see:

    Found CUDA: /usr/local/cuda (found version "10.1")

or:

    Found CUDA: /usr/local/cuda-10.1 (found version "10.1")

Otherwise try with:

    export PATH=/usr/local/cuda-10.1:$PATH


### Building the library from source for release

Build it from source with cmake. `cd` into `libs/darknet/cMake/` and then run:

    mkdir cmake_release
    cd cmake_release
    cmake ..
    make

### Building the library from source for debug

Build it from source with cmake. `cd` into `libs/darknet/cMake/` and then run:

    mkdir cmake_debug
    cd cmake_debug
    cmake -DCMAKE_BUILD_TYPE=Debug ..
    make

### You may have problems with cmake

As an example if you get this lines:

    CMake Error: The following variables are used in this project, but they are set to NOTFOUND.
    Please set them or make sure they are set and tested correctly in the CMake files:
    CUDA_cublas_device_LIBRARY (ADVANCED)

Solve this error installing cmake 3.12.2:

    wget https://cmake.org/files/v3.12/cmake-3.12.2-Linux-x86_64.sh
    sudo sh cmake-3.12.2-Linux-x86_64.sh --prefix=/usr/local --exclude-subdir

### You may have problems with the drivers

When you had previosly downloaded the CUDA toolkit installer there was an option to install the driver.
If using it didn't work, you can try running this commands:

    sudo apt update
    sudo apt install nvdia-driver-xxx-*    # where xxx indicates the actual driver version displayed in the installer option
    sudo apt install xserver-xorg-video-nvdia-xxx    # again, where xxx indicates the actual driver version displayed in the installer option
    
## Credits

* Original Code: https://github.com/pjreddie/darknet
* Help to compile on Windows: https://github.com/AlexeyAB/darknet/
* Help to call from C++: https://github.com/prabindh/darknet

## Reading

 * [tutorial](https://timebutt.github.io/static/how-to-train-yolov2-to-detect-custom-objects/) on training YoloV2 to detect custom objects
